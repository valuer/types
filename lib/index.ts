export type FunctionSpecific<
	ArgumentType = any,
	ReturnType = any,
> = (arg?: ArgumentType) => ReturnType;

export type Thunk<
	ArgumentType = any,
> = FunctionSpecific<ArgumentType, boolean>;

export type Thrower<
	ArgumentType = any,
	ReturnType = ArgumentType,
> = FunctionSpecific<ArgumentType, never | ReturnType>;

export type Class<
	T extends Object = any
> = new (...args: any[]) => T;

export type NumericRange = [ number, number ];

export type Parity = "even" | "odd";
export type TypeOf = "string" | "number" | "boolean" | "symbol" | "undefined" | "object" | "function";
